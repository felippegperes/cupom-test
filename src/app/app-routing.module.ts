import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "categories",
    loadChildren: "./pages/categories/categories.module#CategoriesModule",
  },
  {
    path: "catevouchergories",
    loadChildren: "./pages/voucher/voucher.module#VoucherModule",
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
