import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoucherFormComponent } from './voucher-form/voucher-form.component';

@NgModule({
  declarations: [VoucherFormComponent],
  imports: [
    CommonModule
  ]
})
export class VoucherModule { }
